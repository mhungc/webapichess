﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL
{
    public interface IPutBehavior
    {
         AbstractMessageBody PutPieceOnBoard(AbstractBoard board, AbstractPiece piece);
    }
}
