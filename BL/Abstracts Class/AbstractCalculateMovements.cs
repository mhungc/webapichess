﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public abstract class  AbstractCalculateMovements
    {
        private IMovementsBehavior movementsBehavior;

        public IMovementsBehavior MovementsBehavior
        {
            get { return movementsBehavior; }
            set { movementsBehavior = value; }
        }
    }
}
