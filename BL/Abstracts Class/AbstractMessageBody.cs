﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public abstract class AbstractMessageBody
    {
        private string message;

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        private AbstractPiece piece;

        public AbstractPiece Piece
        {
            get { return piece; }
            set { piece = value; }
        }

        public abstract AbstractMessageBody CreateMessage(string message, Point actualPosition, AbstractPiece piece);

       


    }
}
