﻿using STRUCTURE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DBChess
    {
        private ISaveGame dbBehavior;

        public ISaveGame DbBehavior
        {
            get { return dbBehavior; }
            set { dbBehavior = value; }
        }

        public DBChess(ISaveGame gameBehavior)
        {
            DbBehavior = gameBehavior;
        }

        public void SaveGame(DTOPiece piece)
        {
            dbBehavior.SaveGame(piece);
        }
    }
}
