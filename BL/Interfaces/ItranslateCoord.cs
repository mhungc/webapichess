﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL
{
    public interface ItranslateCoord
    {
        Point TranslateBoardPointToMatriz(int x, int y, int xMaxBoard);
        Point TranslateMatrixPointToBoard(int x, int y, int xMaxBoard);
    }
}
