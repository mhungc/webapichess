﻿using STRUCTURE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public abstract class AbstractPiece
    {
        private List<Point> movementsPosibles = new List<Point>();

        public List<Point> MovementsPosibles
        {
            get { return movementsPosibles; }
            set { movementsPosibles = value; }
        }

        private string aliasPiece;

        public string AliasPiece
        {
            get { return aliasPiece; }
            set { aliasPiece = value; }
        }

        private int xActualPosition;

        public int XActualPosition
        {
            get { return xActualPosition; }
            set { xActualPosition = value; }
        }
        private int yActualPosition;

        public int YActualPosition
        {
            get { return yActualPosition; }
            set { yActualPosition = value; }
        }

        private IMovementsBehavior pieceMovementsBehavior;

        public IMovementsBehavior PieceMovementsBehavior
        {
            get { return pieceMovementsBehavior; }
            set { pieceMovementsBehavior = value; }
        }


        private IMoveBehavior pieceMoveBehavior;

        public IMoveBehavior PieceMoveBehavior
        {
            get { return pieceMoveBehavior; }
            set { pieceMoveBehavior = value; }
        }

        private IPutBehavior piecePutBehavior;

        public IPutBehavior PiecePutBehavior
        {
            get { return piecePutBehavior; }
            set { piecePutBehavior = value; }
        }

        private ISamePieceBehavior isSamePieceBehavior;

        public ISamePieceBehavior IsSamePieceBehavior
        {
            get { return isSamePieceBehavior; }
            set { isSamePieceBehavior = value; }
        }

        private String color;

        public String Color
        {
            get { return color; }
            set { color = value; }
        }


        private IValidCoordBehavior isValidCoordBehavior;

        public IValidCoordBehavior IsValidCoordBehavior
        {
            get { return isValidCoordBehavior; }
            set { isValidCoordBehavior = value; }
        }

        private IMovingPiece movingPieceBehavior;

        public IMovingPiece MovingPieceBehavior
        {
            get { return movingPieceBehavior; }
            set { movingPieceBehavior = value; }
        }


        public List<Point> CalculateMovements(AbstractBoard board, int xInicialPoition, int yInicialPosition, int xFinalPosition, int yFinalPosition)
        {
            return pieceMovementsBehavior.CalculateMovements(this, board, xInicialPoition, yInicialPosition, xFinalPosition, yFinalPosition);
        }

        //public AbstractMessageBody Move(AbstractBoard board, int xNextPosition, int yNextPosition)
        //{
        //    return pieceMoveBehavior.Move(board, this, xNextPosition, yNextPosition);
        //}

        public AbstractMessageBody CanPutOnBoard(AbstractBoard board)
        {
            return piecePutBehavior.PutPieceOnBoard(board, this);
        }

        public bool IsSamePiece(AbstractPiece pieceForCompare, AbstractBoard board)
        {
            return isSamePieceBehavior.IsSamePiece(this, pieceForCompare);
        }

        public bool ValidCoordBehavior(int xActual, int yActual, int xNext, int yNext)
        {
            return isValidCoordBehavior.IsValidCoordBehavior(xActual, yActual, xNext, yNext);
        }

        public AbstractMessageBody Move(AbstractBoard board, int xFinalPosition, int yFinalPosition)
        {
            return movingPieceBehavior.MovingPiece(board, isSamePieceBehavior, this, xFinalPosition, yFinalPosition);
        }


    }
}
