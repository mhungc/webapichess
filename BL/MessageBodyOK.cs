﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class MessageBodyOK : AbstractMessageBody
    {
        private Point actualPosition;

        public Point ActualPosition
        {
            get { return actualPosition; }
            set { actualPosition = value; }
        }

        public MessageBodyOK(string message, Point actualPosition, AbstractPiece piece)
        {
            this.Message = message;
            this.ActualPosition = actualPosition;
            this.Piece = piece;
        }

        public override AbstractMessageBody CreateMessage(string message, Point actualPosition, AbstractPiece piece)
        {
            throw new NotImplementedException();
        }
    }
}
