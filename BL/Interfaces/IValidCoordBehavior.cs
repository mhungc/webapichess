﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL
{
    public interface IValidCoordBehavior
    {
         bool IsValidCoordBehavior(int xActual, int yActual, int xNext, int yNext);
    }
}
