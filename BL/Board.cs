﻿using STRUCTURE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Board : AbstractBoard
    {
        public Board(int x, int y)
        {
            this.BoardWorld = new AbstractPiece[x, y];
            this.XSizeBoard = x - 1;
            this.YSizeBoard = y - 1;
        }

        public override Point TranslateBoardPointToMatriz(int x, int y, int xMaxBoard)
        {
            return this.TranslateCoordBehavior.TranslateBoardPointToMatriz(x, y, xMaxBoard);
        }

        public override Point TranslateMatrixPointToBoard(int x, int y, int xMaxBoard)
        {
            return this.TranslateCoordBehavior.TranslateMatrixPointToBoard(x, y, xMaxBoard);
        }

        public override AbstractPiece CreatePiece(DTOPiece dtoPiece, ISamePieceBehavior samePieceBehavior, IMovingPiece movingPieceBheavior, IValidCoordBehavior validCoordBheavior, IMovementsBehavior movementsBehavior)
        {
            AbstractPiece piece = new Piece()
            {
                AliasPiece = dtoPiece.Alias,
                Color = dtoPiece.Color,
                PiecePutBehavior = null,
                IsSamePieceBehavior = samePieceBehavior,
                MovingPieceBehavior = movingPieceBheavior,
                IsValidCoordBehavior = validCoordBheavior,
                PieceMovementsBehavior = movementsBehavior,
                XActualPosition = dtoPiece.X,
                YActualPosition = dtoPiece.Y
            };

            return piece;
        }

        //public override void PutPieceOnBoard(DTOPiece dtoPiece)
        //{
        //    switch (dtoPiece.Alias)
        //    {
        //        case "T":

        //            Point pointTranslate = TranslateBoardPointToMatriz(dtoPiece.X, dtoPiece.Y, this.xSizeBoard);
        //            dtoPiece.X = pointTranslate.X;
        //            dtoPiece.Y = pointTranslate.Y;

        //            AbstractPiece piece = CreatePiece(dtoPiece, new TowerMovementsBehavior(), new TowerPutBehavior());



        //            this.boardWorld[pointTranslate.X, pointTranslate.Y] = piece;
        //            break;
        //        default:
        //            break;
        //    }
        //}


        public override void PutPieceInBoard(DTOPiece dtoPiece)
        {
            AbstractPiece pieceOnBoard = new Piece()
            {
                AliasPiece = dtoPiece.Alias,
                Color = dtoPiece.Color,
                XActualPosition = dtoPiece.X,
                YActualPosition = dtoPiece.Y
            };
            this.BoardWorld[pieceOnBoard.XActualPosition, pieceOnBoard.YActualPosition] = pieceOnBoard;
        }

        public override void PutPieceOnBoard(AbstractPiece Piece)
        {
            throw new NotImplementedException();
        }
    }
}
