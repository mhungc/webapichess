﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public interface IMovementsBehavior
    {
        List<Point> CalculateMovements(AbstractPiece piece, AbstractBoard board, int xInicialPoition, int yInicialPosition, int xFinalPosition, int yFinalPosition);
    }
}
