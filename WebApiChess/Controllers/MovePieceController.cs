﻿using STRUCTURE;
using BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using DAL;


namespace WebApiChess.Controllers
{
    public class MovePieceController : ApiController
    {
        // GET: api/MovePiece
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/MovePiece/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/MovePiece
        public HttpResponseMessage Post([FromBody]DTOPiece dtoPiece)
        {
            int X = dtoPiece.X;
            int Y = dtoPiece.Y;

            //Create the Board
            AbstractBoard board = new Board(8, 8);
            board.TranslateCoordBehavior = new TranslateCoordBeginLeftButton();

            //PUT hORSE ON BOARD
            DTOPiece horsePiece = new DTOPiece()
            {
                Alias = "H",
                Color = "WHITE",
                X = 1,
                Y = 7
            };
            board.PutPieceInBoard(horsePiece);

            //Create Piece
            AbstractPiece gamePiece = Piece.GetPiece(board, dtoPiece);
            AbstractMessageBody MESSAGE_BODY = gamePiece.Move(board, dtoPiece.xFinal, dtoPiece.yFinal);
            //  string jsonMessage = Newtonsoft.Json.JsonConvert.SerializeObject(MESSAGE_BODY);

            HttpResponseMessage response;
            if (MESSAGE_BODY is MessageBodyOK)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, MESSAGE_BODY);

            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.Forbidden, MESSAGE_BODY);
            }

            DBChess ChessSQL = new DBChess(new GamesRepositoryXML());
            DTOPiece pieceForSave = new DTOPiece()
            {
                X = MESSAGE_BODY.Piece.XActualPosition,
                Y = MESSAGE_BODY.Piece.YActualPosition,
                Alias = MESSAGE_BODY.Piece.AliasPiece
            };

            ChessSQL.SaveGame(pieceForSave);

            return response;
        }

        // PUT: api/MovePiece/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/MovePiece/5
        public void Delete(int id)
        {
        }
    }
}
