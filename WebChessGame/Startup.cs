﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebChessGame.Startup))]
namespace WebChessGame
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
