﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Piece : AbstractPiece
    {
        public static AbstractPiece GetPiece(AbstractBoard board, STRUCTURE.DTOPiece piece)
        {
            AbstractPiece returnPiece;
            switch (piece.Alias)
            {
                case "T":

                    returnPiece = board.CreatePiece(piece,
                                            new SamePieceBehavior(),
                                            new TowerMovingBehavior(),
                                            new TowerCoordBehavior(),
                                            new TowerMovementsBehavior());
                    break;
                case "A":

                    returnPiece = board.CreatePiece(piece,
                                            new SamePieceBehavior(),
                                            new BishopMovingBehavior(),
                                            new TowerCoordBehavior(),
                                            new BishopMovementsBehavior());
                    break;


                default:
                    returnPiece = null;
                    break;
            }

            return returnPiece;
        }
    }
}
