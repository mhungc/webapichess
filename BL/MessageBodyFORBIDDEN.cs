﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class MessageBodyFORBIDDEN : AbstractMessageBody
    {
        private List<Point> positions;

        public List<Point> PosiblePositions
        {
            get { return positions; }
            set { positions = value; }
        }

        public MessageBodyFORBIDDEN(string message, List<Point> posiblesPositions, AbstractPiece piece)
        {
            this.Message = message;
            this.PosiblePositions = posiblesPositions;
            this.Piece = piece;
        }

        public override AbstractMessageBody CreateMessage(string message, Point actualPosition, AbstractPiece piece)
        {
            throw new NotImplementedException();
        }
    }
}
