﻿using STRUCTURE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class GamesRepository : ISaveGame
    {
        DBChessEntities db;

        public GamesRepository()
        {
            this.db = new DBChessEntities();
        }


        public void SaveGame(DTOPiece piece)
        {
            using (var dbChess = db)
            {
                Games game = new Games()
                {
                    ActualPosition = "(" + piece.X + "," + piece.Y + ")",
                    Date = DateTime.Now,
                    PieceType = piece.Alias
                };

                dbChess.Games.Add(game);
                dbChess.SaveChanges();

            }
        }
    }
}
