﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STRUCTURE
{
    public class DTOPiece
    {
        public int X { get; set; }
        public int Y { get; set; }

        public int xFinal { get; set; }
        public int yFinal { get; set; }
        public string Alias { get; set; }
        public string Color { get; set; }


    }
}
