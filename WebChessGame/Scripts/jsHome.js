﻿

$(document).ready(function () {

    $("#clear").click(function () {
        $("#positionActual,#message,#posiblesMovements").empty();
    });

    $("#callChessAPI").click(function () {
        console.debug("click");

        var xActual = $("#Xactual").val();
        var yActual = $("#Yactual").val();
        var xFinal = $("#Xfinal").val();
        var yFinal = $("#Yfinal").val();
        var Color = $("#Color").val();
        var Alias = $("#Alias").val();

        $.ajax({
            cache: false,
            url: "http://localhost:2379/api/MovePiece",
            dataType: "json",
            method: "POST",
            data: {
                x: xActual,
                y: yActual,
                xFinal: xFinal,
                yFinal: yFinal,
                Color: Color,
                Alias: Alias
            },
            success: function (data) {
                console.debug("data..");
                test = data;
                $("#positionActual").html("Actual Position: (" + test.Piece.XActualPosition + "," + test.Piece.YActualPosition + " )");
                $("#message").html("Message: " + test.Message);
            },
            error: function (xhr) {
                console.debug("error..");
                test = xhr;

                $("#positionActual").html("Actual Position: (" + test.responseJSON.Piece.XActualPosition + "," + test.responseJSON.Piece.YActualPosition + " )");
                $("#message").html("Message: " + test.responseJSON.Message);

                var posiblesMovements = $("#posiblesMovements");
                posiblesMovements.append("<div>Posibles movimientos:</div>");
                $.each(test.responseJSON.PosiblePositions, function (index, obj) {
                    posiblesMovements.append("<div>(" + obj.X + "," + obj.Y + ")</div>");
                });

            }
        });
    });
});