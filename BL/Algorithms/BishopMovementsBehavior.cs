﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class BishopMovementsBehavior : IMovementsBehavior
    {
        public List<Point> CalculateMovements(AbstractPiece piece, AbstractBoard board, int xInicialPoition, int yInicialPosition, int xFinalPosition, int yFinalPosition)
        {
            //Calculate Vertical

            int xTmpInicialPosition = xInicialPoition;
            int yTmpInicialPosition = yInicialPosition;
            while (yTmpInicialPosition < board.YSizeBoard)//Move UP Right diagonal
            {
                int XpositionMoved = xTmpInicialPosition - 1;
                int YpositionMoved = yTmpInicialPosition + 1;
                AbstractPiece pieceCompare = board.BoardWorld[XpositionMoved, YpositionMoved];
                if (!piece.IsSamePieceBehavior.IsSamePiece(piece, pieceCompare))
                {
                    //Save Position
                    Point point = new Point();
                    point.X = XpositionMoved;
                    point.Y = YpositionMoved;
                    piece.MovementsPosibles.Add(board.TranslateMatrixPointToBoard(point.X, point.Y, board.XSizeBoard));
                    xTmpInicialPosition = XpositionMoved;
                    yTmpInicialPosition = YpositionMoved;
                }
                else
                {
                    break;
                }
            }

            xTmpInicialPosition = xInicialPoition;
            yTmpInicialPosition = yInicialPosition;
            while (yTmpInicialPosition < 0)//Move UP left diagonal
            {
                int XpositionMoved = xTmpInicialPosition - 1;
                int YpositionMoved = yTmpInicialPosition - 1;
                AbstractPiece pieceCompare = board.BoardWorld[XpositionMoved, YpositionMoved];
                if (!piece.IsSamePieceBehavior.IsSamePiece(piece, pieceCompare))
                {
                    //Save Position
                    Point point = new Point();
                    point.X = XpositionMoved;
                    point.Y = YpositionMoved;
                    piece.MovementsPosibles.Add(board.TranslateMatrixPointToBoard(point.X, point.Y, board.XSizeBoard));
                    xTmpInicialPosition = XpositionMoved;
                    yTmpInicialPosition = YpositionMoved;
                }
                else
                {
                    break;
                }
            }

            xTmpInicialPosition = xInicialPoition;
            yTmpInicialPosition = yInicialPosition;
            while (yTmpInicialPosition < board.YSizeBoard)//Move Down Right diagonal
            {
                int XpositionMoved = xTmpInicialPosition + 1;
                int YpositionMoved = yTmpInicialPosition + 1;
                AbstractPiece pieceCompare = board.BoardWorld[XpositionMoved, YpositionMoved];
                if (!piece.IsSamePieceBehavior.IsSamePiece(piece, pieceCompare))
                {
                    //Save Position
                    Point point = new Point();
                    point.X = XpositionMoved;
                    point.Y = YpositionMoved;
                    piece.MovementsPosibles.Add(board.TranslateMatrixPointToBoard(point.X, point.Y, board.XSizeBoard));
                    xTmpInicialPosition = XpositionMoved;
                    yTmpInicialPosition = YpositionMoved;
                }
                else
                {
                    break;
                }
            }

            xTmpInicialPosition = xInicialPoition;
            yTmpInicialPosition = yInicialPosition;
            while (xTmpInicialPosition < board.XSizeBoard)//Move Down Left diagonal
            {
                int XpositionMoved = xTmpInicialPosition + 1;
                int YpositionMoved = yTmpInicialPosition - 1;
                AbstractPiece pieceCompare = board.BoardWorld[XpositionMoved, YpositionMoved];
                if (!piece.IsSamePieceBehavior.IsSamePiece(piece, pieceCompare))
                {
                    //Save Position
                    Point point = new Point();
                    point.X = XpositionMoved;
                    point.Y = YpositionMoved;
                    piece.MovementsPosibles.Add(board.TranslateMatrixPointToBoard(point.X, point.Y, board.XSizeBoard));
                    xTmpInicialPosition = XpositionMoved;
                    yTmpInicialPosition = YpositionMoved;
                }
                else
                {
                    break;
                }
            }

            return piece.MovementsPosibles;
        }
    }
}
