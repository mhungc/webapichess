﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class TowerCoordBehavior : IValidCoordBehavior
    {
        public bool IsValidCoordBehavior(int xActual, int yActual, int xNext, int yNext)
        {
            //y1 -y2 = m(x1-x2)
            int m = (xActual - xNext) / (yActual - yNext);

            if (m != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
