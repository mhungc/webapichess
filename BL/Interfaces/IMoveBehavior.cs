﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL
{
    public interface IMoveBehavior
    {
         AbstractMessageBody Move(AbstractBoard board, AbstractPiece piece, int xNextPosition, int yNextPosition);

    }
}
