﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class TowerMovementsBehavior : IMovementsBehavior
    {
        public List<Point> CalculateMovements(AbstractPiece piece, AbstractBoard board, int xInicialPoition, int yInicialPosition, int xFinalPosition, int yFinalPosition)
        {
            //Calculate Vertical

            int tmpInicialPosition = xInicialPoition;
            while (tmpInicialPosition > 0)//Move UP
            {
                int positionMoved = tmpInicialPosition - 1;
                AbstractPiece pieceCompare = board.BoardWorld[positionMoved, yFinalPosition];
                if (!piece.IsSamePieceBehavior.IsSamePiece(piece, pieceCompare))
                {
                    //Save Position
                    Point point = new Point();
                    point.X = positionMoved;
                    point.Y = yInicialPosition;
                    piece.MovementsPosibles.Add(board.TranslateMatrixPointToBoard(point.X, point.Y, board.XSizeBoard));
                    tmpInicialPosition = positionMoved;
                }
                else
                {
                    break;
                }
            }

            tmpInicialPosition = xInicialPoition;
            while (tmpInicialPosition < board.XSizeBoard)//Move DOWN
            {
                int positionMoved = tmpInicialPosition + 1;
                AbstractPiece pieceCompare = board.BoardWorld[positionMoved, yFinalPosition];
                if (!piece.IsSamePieceBehavior.IsSamePiece(piece, pieceCompare))
                {
                    //Save Position
                    Point point = new Point();
                    point.X = positionMoved;
                    point.Y = yInicialPosition;
                    piece.MovementsPosibles.Add(board.TranslateMatrixPointToBoard(point.X, point.Y, board.XSizeBoard));
                    tmpInicialPosition = positionMoved;
                }
                else
                {
                    break;
                }
            }

            //Calculate Horizontal
            tmpInicialPosition = yInicialPosition;
            while (tmpInicialPosition < board.YSizeBoard)//Right
            {
                int positionMoved = tmpInicialPosition + 1;
                AbstractPiece pieceCompare = board.BoardWorld[xFinalPosition, positionMoved];
                if (!piece.IsSamePieceBehavior.IsSamePiece(piece, pieceCompare))
                {
                    //Save Position
                    Point point = new Point();
                    point.X = xInicialPoition;
                    point.Y = positionMoved;
                    piece.MovementsPosibles.Add(board.TranslateMatrixPointToBoard(point.X, point.Y, board.XSizeBoard));
                    tmpInicialPosition = positionMoved;
                }
                else
                {
                    break;
                }
            }

            tmpInicialPosition = yInicialPosition;
            while (tmpInicialPosition > 0) //left
            {
                int positionMoved = tmpInicialPosition - 1;
                AbstractPiece pieceCompare = board.BoardWorld[xFinalPosition, positionMoved];
                if (!piece.IsSamePieceBehavior.IsSamePiece(piece, pieceCompare))
                {
                    //Save Position
                    Point point = new Point();
                    point.X = xInicialPoition;
                    point.Y = positionMoved;
                    piece.MovementsPosibles.Add(board.TranslateMatrixPointToBoard(point.X, point.Y, board.XSizeBoard));
                    tmpInicialPosition = positionMoved;
                }
                else
                {
                    break;
                }
            }

            return piece.MovementsPosibles;
        }
    }
}
