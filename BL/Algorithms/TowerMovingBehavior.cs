﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class TowerMovingBehavior : IMovingPiece
    {
        public AbstractMessageBody MoveDown(AbstractPiece piece, ISamePieceBehavior isSamePieceBehavior, AbstractBoard board, int xFinalPosition, int yFinalPosition)
        {

            int inicialXPosition = piece.XActualPosition;
            int inicialYPosition = piece.YActualPosition;

            bool wasMovedCompleted = true;
            while (piece.XActualPosition < xFinalPosition)
            {
                int xMoved = piece.XActualPosition + 1;
                AbstractPiece pieceCompare = board.BoardWorld[xMoved, yFinalPosition];
                if (!isSamePieceBehavior.IsSamePiece(piece, pieceCompare))
                {
                    piece.XActualPosition = xMoved;
                }
                else
                {
                    wasMovedCompleted = false;
                    break;
                }
            }

            Point point = board.TranslateMatrixPointToBoard(piece.XActualPosition, piece.YActualPosition, board.XSizeBoard);
            piece.XActualPosition = point.X;
            piece.YActualPosition = point.Y;

            if (wasMovedCompleted)
            {

                return new MessageBodyOK("Movido completo la pieza",
                                            new Point()
                                            {
                                                X = point.X,
                                                Y = point.Y
                                            },
                                            piece);
            }
            else
            {
                return new MessageBodyFORBIDDEN("Movido hasta ese punto la pieza",
                                               piece.CalculateMovements(board, inicialXPosition, inicialYPosition, xFinalPosition, yFinalPosition),
                                               piece);
            }
        }


        public AbstractMessageBody MoveUP(AbstractPiece piece, ISamePieceBehavior isSamePieceBehavior, AbstractBoard board, int xFinalPosition, int yFinalPosition)
        {
            int inicialXPosition = piece.XActualPosition;
            int inicialYPosition = piece.YActualPosition;

            bool wasMovedCompleted = true;
            while (piece.XActualPosition > xFinalPosition)
            {
                int xMoved = piece.XActualPosition - 1;
                AbstractPiece pieceCompare = board.BoardWorld[xMoved, yFinalPosition];
                if (!isSamePieceBehavior.IsSamePiece(piece, pieceCompare))
                {
                    piece.XActualPosition = xMoved;
                }
                else
                {
                    wasMovedCompleted = false;
                    break;
                }
            }

            Point point = board.TranslateMatrixPointToBoard(piece.XActualPosition, piece.YActualPosition, board.XSizeBoard);
            piece.XActualPosition = point.X;
            piece.YActualPosition = point.Y;

            if (wasMovedCompleted)
            {

                return new MessageBodyOK("Movido completo la pieza",
                                            new Point()
                                            {
                                                X = point.X,
                                                Y = point.Y
                                            },
                                            piece);
            }
            else
            {
                return new MessageBodyFORBIDDEN("Movido hasta ese punto la pieza",
                                               piece.CalculateMovements(board, inicialXPosition, inicialYPosition, xFinalPosition, yFinalPosition),
                                               piece);
            }
        }

        public AbstractMessageBody MoveRight(AbstractPiece piece, ISamePieceBehavior isSamePieceBehavior, AbstractBoard board, int xFinalPosition, int yFinalPosition)
        {
            int inicialXPosition = piece.XActualPosition;
            int inicialYPosition = piece.YActualPosition;
            
            bool wasMovedCompleted = true;
            while (piece.YActualPosition < yFinalPosition)
            {
                int yMoved = piece.YActualPosition + 1;
                AbstractPiece pieceCompare = board.BoardWorld[piece.XActualPosition, yMoved];
                if (!isSamePieceBehavior.IsSamePiece(piece, pieceCompare))
                {
                    piece.YActualPosition = yMoved;
                }
                else
                {
                    wasMovedCompleted = false;
                    break;
                }
            }

            Point point = board.TranslateMatrixPointToBoard(piece.XActualPosition, piece.YActualPosition, board.XSizeBoard);
            piece.XActualPosition = point.X;
            piece.YActualPosition = point.Y;

            if (wasMovedCompleted)
            {

                return new MessageBodyOK("Movido completo la pieza",
                                            new Point()
                                            {
                                                X = point.X,
                                                Y = point.Y
                                            },
                                            piece);
            }
            else
            {
                return new MessageBodyFORBIDDEN("Movido hasta ese punto la pieza",
                                               piece.CalculateMovements(board, inicialXPosition, inicialYPosition, xFinalPosition, yFinalPosition),
                                               piece);
            }
        }

        public AbstractMessageBody MoveLeft(AbstractPiece piece, ISamePieceBehavior isSamePieceBehavior, AbstractBoard board, int xFinalPosition, int yFinalPosition)
        {
            int inicialXPosition = piece.XActualPosition;
            int inicialYPosition = piece.YActualPosition;

            bool wasMovedCompleted = true;
            while (piece.YActualPosition > yFinalPosition)
            {
                int yMoved = piece.YActualPosition - 1;
                AbstractPiece pieceCompare = board.BoardWorld[piece.XActualPosition, yMoved];
                if (!isSamePieceBehavior.IsSamePiece(piece, pieceCompare))
                {
                    piece.YActualPosition = yMoved;
                }
                else
                {
                    wasMovedCompleted = false;
                    break;
                }
            }

            Point point = board.TranslateMatrixPointToBoard(piece.XActualPosition, piece.YActualPosition, board.XSizeBoard);

            piece.XActualPosition = point.X;
            piece.YActualPosition = point.Y;

            if (wasMovedCompleted)
            {

                return new MessageBodyOK("Movido completo la pieza",
                                            new Point()
                                            {
                                                X = point.X,
                                                Y = point.Y
                                            },
                                            piece);
            }
            else
            {
                return new MessageBodyFORBIDDEN("No se puede mover la pieza hasta ese punto (" + xFinalPosition + "," + yFinalPosition + ")",
                                               piece.CalculateMovements(board, inicialXPosition, inicialYPosition, xFinalPosition, yFinalPosition),
                                               piece);
            }
        }

        public AbstractMessageBody MovingPiece(AbstractBoard board, ISamePieceBehavior isSamePieceBehavior, AbstractPiece piece, int xFinalPosition, int yFinalPosition)
        {
            Point pointACTUAL = board.TranslateBoardPointToMatriz(piece.XActualPosition, piece.YActualPosition, board.XSizeBoard);
            piece.XActualPosition = pointACTUAL.X;
            piece.YActualPosition = pointACTUAL.Y;

            Point pointFINAL = board.TranslateBoardPointToMatriz(xFinalPosition, yFinalPosition, board.XSizeBoard);
            xFinalPosition = pointFINAL.X;
            yFinalPosition = pointFINAL.Y;

            if (piece.YActualPosition == yFinalPosition) //Move Vertical
            {
                if (piece.XActualPosition - xFinalPosition > 0) //Move UP
                {
                    return MoveUP(piece, isSamePieceBehavior, board, xFinalPosition, yFinalPosition);
                }
                else //Move DOWN
                {
                    return MoveDown(piece, isSamePieceBehavior, board, xFinalPosition, yFinalPosition);
                }
            }
            else if (piece.XActualPosition == xFinalPosition) // Move Horizontal
            {
                if (piece.YActualPosition - yFinalPosition < 0) //Move Right
                {
                    return MoveRight(piece, isSamePieceBehavior, board, xFinalPosition, yFinalPosition);
                }
                else
                {
                    return MoveLeft(piece, isSamePieceBehavior, board, xFinalPosition, yFinalPosition);
                }
            }
            else
            {
                return null;
            }
        }
    }
}
