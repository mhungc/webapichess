﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class SamePieceBehavior : ISamePieceBehavior
    {
        public bool IsSamePiece(AbstractPiece piece, AbstractPiece PieceForCompare)
        {
            if (PieceForCompare != null)
            {
                if (piece.Color == PieceForCompare.Color)
                {
                    return true;
                }
            }

            return false;

        }
    }
}
