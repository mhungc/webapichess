﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Configuration;

namespace DAL
{
    public class GamesRepositoryXML : ISaveGame
    {
        public void SaveGame(STRUCTURE.DTOPiece piece)
        {
            string url_XmlDbGames = ConfigurationSettings.AppSettings["URLxmlDB"];

            if (!File.Exists(url_XmlDbGames))
            {
                using (XmlWriter writer = XmlWriter.Create(url_XmlDbGames))
                {
                    writer.WriteStartElement("DataGame");

                    writer.WriteStartElement("game");
                    writer.WriteElementString("Date", DateTime.Now.ToShortDateString());
                    writer.WriteElementString("X", Convert.ToString(piece.X));
                    writer.WriteElementString("Y", Convert.ToString(piece.Y));
                    writer.WriteElementString("Piece", piece.Alias.ToString());

                    writer.WriteEndDocument();
                }
            }
            else
            {
                FileStream file = new FileStream(url_XmlDbGames, FileMode.Open);
                XmlDocument xd = new XmlDocument();
                xd.Load(file);

                XmlElement xmlEGame = xd.CreateElement("game");

                //Date
                XmlElement xmlDate = xd.CreateElement("Date");
                XmlText xmlDateNode = xd.CreateTextNode(DateTime.Now.ToShortDateString());
                xmlDate.AppendChild(xmlDateNode);
                xmlEGame.AppendChild(xmlDate);

                //X
                XmlElement xmlX = xd.CreateElement("X");
                XmlText xmlXNode = xd.CreateTextNode(Convert.ToString(piece.X));
                xmlX.AppendChild(xmlXNode);
                xmlEGame.AppendChild(xmlX);

                //Y
                XmlElement xmlY = xd.CreateElement("Y");
                XmlText xmlYNode = xd.CreateTextNode(Convert.ToString(piece.Y));
                xmlY.AppendChild(xmlYNode);
                xmlEGame.AppendChild(xmlY);

                //ALIAS
                XmlElement xmlAlias = xd.CreateElement("Piece");
                XmlText xmlAliasNode = xd.CreateTextNode(piece.Alias);
                xmlAlias.AppendChild(xmlAliasNode);
                xmlEGame.AppendChild(xmlAlias);

                xd.DocumentElement.AppendChild(xmlEGame);

                file.Close();
                xd.Save("C:/Users/Marco/Documents/Visual Studio 2013/Projects/WebApiChess/DAL/dbxML/Games.xml");

            }
        }
    }
}
