﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public interface IMovingPiece
    {
         AbstractMessageBody MovingPiece(AbstractBoard board, ISamePieceBehavior isSamePieceBehavior, AbstractPiece piece, int xFinalPosition, int yFinalPosition);
    }
}
