﻿using STRUCTURE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public abstract class AbstractBoard
    {
        private AbstractPiece[,] boardWorld;

        public AbstractPiece[,] BoardWorld
        {
            get { return boardWorld; }
            set { boardWorld = value; }
        }

        private int xSizeBoard;

        public int XSizeBoard
        {
            get { return xSizeBoard; }
            set { xSizeBoard = value; }
        }

        private int ySizeBoard;

        public int YSizeBoard
        {
            get { return ySizeBoard; }
            set { ySizeBoard = value; }
        }

        private ItranslateCoord translateCoordBehavior;

        public ItranslateCoord TranslateCoordBehavior
        {
            get { return translateCoordBehavior; }
            set { translateCoordBehavior = value; }
        }

        public abstract Point TranslateBoardPointToMatriz(int x, int y, int xMaxBoard);

        public abstract Point TranslateMatrixPointToBoard(int x, int y, int xMaxBoard);

        public abstract void PutPieceOnBoard(AbstractPiece Piece);

        public abstract AbstractPiece CreatePiece(DTOPiece dtoPiece, ISamePieceBehavior samePieceBehavior, IMovingPiece movingPieceBheavior, IValidCoordBehavior validCoordBheavior, IMovementsBehavior movementsBehavior);

        public abstract void PutPieceInBoard(DTOPiece dtoPiece);
    }
}
