﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class TranslateCoordBeginLeftButton : ItranslateCoord
    {
        public Point TranslateBoardPointToMatriz(int x, int y, int xMaxBoard)
        {
            Point point = new Point();

            //int xTranslate = xMaxBoard - x;
            //point.X = xTranslate;
            //point.Y = y;
            point.X = xMaxBoard - y;
            point.Y = x;


            return point;

        }

        public Point TranslateMatrixPointToBoard(int x, int y, int xMaxBoard)
        {
            Point point = new Point();

            //int xTranslate = xMaxBoard - x;
            //point.X = xTranslate;
            //point.Y = y;
            point.X = y;
            point.Y =  xMaxBoard - x ;


            return point;
        }
    }
}
